import { Component } from '@angular/core';

@Component({
    selector: 'meu-primeiro-component',
    templateUrl: './meu-primeiro.component.html'
    // Outra Possibilidade
    // template: '<h1>teste de Criacao de Componente - 2</h1>'
})

export class MeuPrimeiroComponent {}